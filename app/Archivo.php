<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    protected $fillable=['user_id', 'archivo'];

    public function user(){
        return $this->belongsTo( 'App\User', 'user_id');
    }
}
