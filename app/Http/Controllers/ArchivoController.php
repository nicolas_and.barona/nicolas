<?php

namespace App\Http\Controllers;

use App\Archivo;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


class ArchivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $archivos = Archivo::where('user_id', Auth::id())->latest()->get();
        return view('pages.archivos', compact('archivos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $max_size = (int)ini_get('upload_max_files') * 10240;

        if ($request->hasFile('archivo')) {
            $file = $request->file('archivo');
            Storage::delete(public_path('/archivos').$file->getClientOriginalName());
            $source =  public_path('/archivos');

            $file->move($source, $file->getClientOriginalName());
            $archivo = new Archivo();
            $archivo->user_id = Auth::user()->id;
            $archivo->archivo = $file->getClientOriginalName();
    
            $archivo->save();
    
            Alert::success('Listo', '¡Se ha cargado el archivo con exito!');
        }else{
            Alert::error('Error', '¡Debe seleccionar un archivo para continuar!');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Archivo  $archivo
     * @return \Illuminate\Http\Response
     */
    public function show(Archivo $archivo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Archivo  $archivo
     * @return \Illuminate\Http\Response
     */
    public function edit(Archivo $archivo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Archivo  $archivo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Archivo $archivo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Archivo  $archivo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $archivo = Archivo::whereId($id)->firstOrFail();
        unlink(public_path('archivos'.'/'.$archivo->archivo));
        $archivo->delete();

        Alert::info('Listo', '¡Se ha eliminado el archivo con exito!');
        return back();
    }
}
