<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nicolas</title>
    </head>
<body>
    <main class="py-4">
    @if(Route::has('login'))
        @auth
            @include('pages.inicio')
        @else
            @if(Route::current()->uri() == "register")
                @include('auth.register')
            @else
                @include('auth.login')
            @endif   
        @endauth
    @endif
    </main>   
</body>
</html>