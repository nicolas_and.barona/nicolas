@extends('layouts.app')

<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <i class="fas fa-caret-square-up"></i>
            Sistema Carga de Archivos
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                <a class="dropdown-item" href="{{ url('/') }}">
                    <i class="fas fa-upload"></i>
                     {{ __('Cargar') }}
                </a>
                <a class="dropdown-item" href="{{ route('pages.archivos') }}">
                    <i class="fas fa-file-import"></i>
                     {{ __('Archivos') }}
                </a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt "></i>
                         {{ __('Salir') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>

                @endguest
            </ul>
        </div>
    </div>
</nav>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="/cargar" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header">Mis Archivos </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">Nombre del Archivo</th>
                                <th scope="col">Ver</th>
                                <th scope="col">Eliminar</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($archivos as $archivo)
                                <tr>
                                    <td>{{$archivo->archivo}}</td>
                                    <td>
                                        <a href="archivos/{{ $archivo->archivo}}" class="btn btn-outline-success">Ver</a>
                                    </td>
                                    <td>
                                        <form action="{{ route('pages.destroy', $archivo->id)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>