@extends('layouts.app')

<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <i class="fas fa-caret-square-up"></i>
            Sistema Carga de Archivos
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                <a class="dropdown-item" href="{{ url('/') }}">
                    <i class="fas fa-upload"></i>
                     {{ __('Cargar') }}
                </a>
                <a class="dropdown-item" href="{{ route('pages.archivos') }}">
                    <i class="fas fa-file-import"></i>
                     {{ __('Archivos') }}
                </a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt "></i>
                         {{ __('Salir') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>

                @endguest
            </ul>
        </div>
    </div>
</nav>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="/cargar" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header">Cargar Archivo </div>
                    <div class="card-body">
                        <div class="col form-group">
                            <label>Archivo:</label>
                            <br>
                            <input type="file"name="archivo" >
                        </div>
                        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                            <button type="submit" class="btn btn-primary me-md-2">
                                <i class="fas fa-upload"></i> Enviar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>